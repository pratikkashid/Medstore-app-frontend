import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  constructor(private productService:ProductService,
   private route:Router) { }

  ngOnInit(): void {
    this.getProducts();
  }
  private getProducts() {
   this.productService.getProductList().subscribe(data=>{
     this.products=data;
     
   })
  }
  addProduct(){
    this.route.navigate(['/AddProduct']);
  }
  productList(){
    this.route.navigate(['/getall']);
  }
  
 redirectToEdit(id:number){
   this.route.navigate(['/editproduct',id]);
 }

 redirectTodelete(id:number){
   this.route.navigate(['/deleteproduct',id]);
 }
 Home() {
  this.route.navigate(['home']);
}

dealer() {
  this.route.navigate(['dealer']);
}
bill() {
  this.route.navigate(['search']);
}
clientList() {
  this.route.navigate(['client/clients']);
}
}
