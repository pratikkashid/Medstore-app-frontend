import { Component, OnInit } from '@angular/core';
import { Dealer } from '../dealer';
import { ActivatedRoute, Router } from '@angular/router';
import { DealerService } from '../dealer.service';
@Component({
  selector: 'app-dealer-details',
  templateUrl: './dealer-details.component.html',
  styleUrls: ['./dealer-details.component.css']
})
export class DealerDetailsComponent implements OnInit {

  dealerId:number;
  dealer:Dealer;
  constructor(private route:ActivatedRoute,
    private dealerService:DealerService) { }

  ngOnInit(): void {
    this.dealerId=this.route.snapshot.params['dealerId'];
    this.dealer=new Dealer();
    this.dealerService.getDealerById(this.dealerId).subscribe(data =>{
      this.dealer=data;
    })
  }

}
