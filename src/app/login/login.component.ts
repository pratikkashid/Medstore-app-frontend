import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: Login = new Login();
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  onSubmit(){
   if(this.login.userName =="admin" && this.login.password=="admin"){
    this.router.navigate(['home']);
   }
  }
}
