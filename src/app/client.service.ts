import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from './client';


@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private getURL = "http://localhost:8084/client/clients";
  private addURL = "http://localhost:8084/client/addClient";
  private deleteURL = "http://localhost:8084/client/deleteClient";
  private getbyidURL = "http://localhost:8084/client/getbyid";

  constructor(private httpClient:HttpClient) { }
 
  getClientList(): Observable<Client[]>{
    return this.httpClient.get<Client[]>(this.getURL);
  }

  createClient(client: Client): Observable<Object>{
    return this.httpClient.post(this.addURL, client);
  }
  deleteClient(id: number):Observable<Object>{
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }

  getClientById(id:number):Observable<Client>{
    return this.httpClient.get<Client>(`${this.getbyidURL}/${id}`);
  }
 
}
