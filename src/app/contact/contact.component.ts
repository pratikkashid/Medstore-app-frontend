import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm!: FormGroup;
disabledSubmitButton: boolean = true;
  optionsSelect!: Array<any>;
  
  constructor(private fb: FormBuilder,private router: Router) {

    this.contactForm = fb.group({
      'contactFormName': ['', Validators.required],
      'contactFormEmail': ['', Validators.compose([Validators.required, Validators.email])],
      'contactFormSubjects': ['', Validators.required],
      'contactFormMessage': ['', Validators.required],
      'contactFormCopy': [''],
      });
    }
    @HostListener('input') oninput() {

      if (this.contactForm.valid) {
        this.disabledSubmitButton = false;
        }
      }

  ngOnInit(): void {
  }

  onSubmit() {
   console.log(this.contactForm);
  }
  Home(){
    this.router.navigate(['home']);
  }
}
