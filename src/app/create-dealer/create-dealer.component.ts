import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dealer } from '../dealer';
import { DealerService } from '../dealer.service';

@Component({
  selector: 'app-create-dealer',
  templateUrl: './create-dealer.component.html',
  styleUrls: ['./create-dealer.component.css']
})
export class CreateDealerComponent implements OnInit {

  dealer: Dealer=new Dealer();
  constructor(private dealerService:DealerService,
    private router:Router) { }

  ngOnInit(): void {
  }
  saveDealer(){
    this.dealerService.createDealer(this.dealer).subscribe(data=>{
      console.log(data);
      this.goToDealerList();
    },
    error=>console.log(error));
  }
  goToDealerList(){
    this.router.navigate(['dealer'])

  }
  onSubmit(){
    console.log(this.dealer);
    this.saveDealer();
  }
}
