import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dealer } from './dealer';



@Injectable({
  providedIn: 'root'
})
export class DealerService {

  private getURL="http://localhost:8084/dealer/getAll";
  private addURL = "http://localhost:8084/dealer/add";
  private deleteURL = "http://localhost:8084/dealer/delete";
  private getbyidURL = "http://localhost:8084/dealer/singleRecord";
  private updateURL= "http://localhost:8084/dealer/update";
  constructor(private httpClient:HttpClient) { }

  getDealerList():Observable<Dealer[]>{
    return this.httpClient.get<Dealer[]>(this.getURL);
  }
  createDealer(dealer:Dealer):Observable<any>{
    return this.httpClient.post(this.addURL,dealer);
  }
  getDealerById(dealerId:number):Observable<Dealer>{
    return this.httpClient.get<Dealer>(`${this.getbyidURL}/${dealerId}`);
  }
  updateDealer(dealerId:number,dealer:Dealer):Observable<Object>{
    return this.httpClient.put(`${this.updateURL}/${dealerId}`,dealer);
  }
  deleteDealer(dealerId:number):Observable<Object>{
    return this.httpClient.delete(`${this.deleteURL}/${dealerId}`);
  }

}
