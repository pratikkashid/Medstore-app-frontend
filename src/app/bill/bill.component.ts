import { Component, OnInit } from '@angular/core';

import * as pdfFonts from "pdfmake/build/vfs_fonts";

import * as pdfMake from 'pdfmake/build/pdfmake';

import { Product } from '../product';
import { Invoice } from '../invoice';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})


export class BillComponent implements OnInit {
  // products: Product[] = [];
  invoice = new Invoice();
  constructor(){
    // Initially one empty product row we will show 
    // this.products.push(new Product());
  }


  ngOnInit(): void {
  }

   
  /* generatePDF(action = 'open') {
    let docDefinition = {
      content: [
        {
          text: 'MEDSTORE SHOP',
          fontSize: 16,
          alignment: 'center',
          color: '#047886'
        },
        {
          text: 'INVOICE',
          fontSize: 20,
          bold: true,
          alignment: 'center',
          decoration: 'underline',
          color: 'skyblue'
        },
        {
          text: 'Customer Details',
          style: 'sectionHeader'
        },
        {
          columns: [
            [
              {
                text: this.invoice.client.clientName,
                bold:true
              },
             
              { text: this.invoice.client.clientEmail },
              { text: this.invoice.client.clientContact }
            ],
            [
              {
                text: `Date: ${new Date().toLocaleString()}`,
                alignment: 'right'
              },
              { 
                text: `Bill No : ${this.invoice.invoiceNo}`,
                alignment: 'right'
              }
            ]
          ]
        },
        {
          text: 'Order Details',
          style: 'sectionHeader'
        },
        {
          table: {
            headerRows: 1,
            widths: ['*', 'auto', 'auto', 'auto'],
            body: [
              ['Product', 'Price', 'Quantity', 'Amount'],
              ...this.invoice.products.map((p: { name: string; price: number; qty: number; }) => ([p.name, p.price, p.qty, (p.price*p.qty).toFixed(2)])),
              [{text: 'Total Amount', colSpan: 3}, {}, {}, this.invoice.products.reduce((sum: number, p: { qty: number; price: number; })=> sum + (p.qty * p.price), 0).toFixed(2)]
            ]
          }
        },
        // {
        //   text: 'Additional Details',
        //   style: 'sectionHeader'
        // },
        // {
        //     text: this.invoice.additionalDetails,
        //     margin: [0, 0 ,0, 15]          
        // },
        {
          columns: [
            [{ qr: `${this.invoice.client.clientName}`, fit: '50' }],
            [{ text: 'Signature', alignment: 'right', italics: true}],
          ]
        },
        {
          text: 'Terms and Conditions',
          style: 'sectionHeader'
        },
        {
            ul: [
              
              'This is system generated invoice.',
            ],
        }
      ],
      styles: {
        sectionHeader: {
          bold: true,
          decoration: 'underline',
          fontSize: 14,
          margin: [0, 15,0, 15]          
        }
      }
    };

    // if(action==='download'){
    //   pdfMake.createPdf(docDefinition).download();
    // }else if(action === 'print'){
    //   pdfMake.createPdf(docDefinition).print();      
    // }else{
    //   pdfMake.createPdf(docDefinition).open();      
    // }

  }
  addProduct(){
    this.invoice.products.push(new Product());
  } */
}
