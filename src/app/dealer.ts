export class Dealer {

    dealerId: number;
    dealerName: string;
    email: string;
    contactNumber: number;

}
