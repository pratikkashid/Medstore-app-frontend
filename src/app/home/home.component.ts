import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Sidebar } from 'ng-sidebar';
import { SidebarComponent } from '@syncfusion/ej2-angular-navigations';
import { ViewChild } from '@angular/core';
import { NavigateSidebarService } from '../navigate-sidebar.service';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products: Product[] = [];
  constructor(private router: Router, private navigateSidebar: NavigateSidebarService,
    private productService:ProductService) { }

  ngOnInit(): void {
    this.getProducts();
  }
  private getProducts() {
    this.productService.getProductList().subscribe(data=>{
      this.products=data;
      
    })
   }


  title = 'medstore-frontend';




  @ViewChild('sidebar')
  sidebar!: SidebarComponent;


  public get showSideBar(): boolean {
    return this.navigateSidebar.showSidebar;
  }

  fun() {

    this.sidebar.toggle();


  }
 


  clientList() {
    this.router.navigate(['client/clients']);
  }
  login() {
    this.router.navigate(['login']);
  }
  product() {
    this.router.navigate(['getall']);
  }
  dealer() {
    this.router.navigate(['dealer']);
  }
  bill() {
    this.router.navigate(['search']);
  }
  LogOut(){
    this.router.navigate(['login']);
  }
  Home(){
    this.router.navigate(['home']);
  }
  addProduct(){
    this.router.navigate(['/AddProduct']);
  }
  productList(){
    this.router.navigate(['/getall']);
  }

  contactus(){
    this.router.navigate(['/contactus']);
  }
  
 redirectToEdit(id:number){
   this.router.navigate(['/editproduct',id]);
 }

 redirectTodelete(id:number){
   this.router.navigate(['/deleteproduct',id]);
 }
}
