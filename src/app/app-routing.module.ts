
import { createComponent } from '@angular/compiler/src/core';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientListComponent } from './client-list/client-list.component';
import { CreateClientComponent } from './create-client/create-client.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

import { AddProductComponent } from './add-product/add-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { ProductListComponent } from './product-list/product-list.component';


import { DealerDetailsComponent } from './dealer-details/dealer-details.component';
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { CreateDealerComponent } from './create-dealer/create-dealer.component';
import { UpdateDealerComponent } from './update-dealer/update-dealer.component';
import { BillComponent } from './bill/bill.component';
import { SearchComponent } from './search/search.component';
import { ContactComponent } from './contact/contact.component';


const routes: Routes = [
  { path:'home', component: HomeComponent},
  { path:'login', component: LoginComponent},
  { path:'client/clients', component: ClientListComponent},
  { path:'client/addClient', component: CreateClientComponent},
  { path:'client/addClient/:id', component: CreateClientComponent},
  { path:'', redirectTo: 'login', pathMatch: 'full' },



  {path:'getall' ,component:ProductListComponent},
  // {path:'',redirectTo:'getall',pathMatch:'full'},
  {path:'AddProduct' , component:AddProductComponent},
  {path:'editproduct/:id', component:EditproductComponent},
  {path:'deleteproduct/:id', component:DeleteProductComponent},



  {path:'dealer',component:DealerListComponent},
  {path:'create-dealer',component:CreateDealerComponent},
  //{path:'',redirectTo:'dealer',pathMatch:'full'},
  {path:'update-dealer/:id',component:UpdateDealerComponent},
  {path :'dealer-details/:id',component:DealerDetailsComponent},
  {path:'bill',component:BillComponent},
  {path:'search',component:SearchComponent},
  {path:'contactus',component:ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
