import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { ClientService } from '../client.service';
import { createComponent } from '@angular/compiler/src/core';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  clients: Client[]=[];
 

  constructor(private clientService: ClientService, private router: Router) {
   
   }

  ngOnInit(): void {
    this.getClients();
  }

 
  deleteClient(id: number){
    this.clientService.deleteClient(id).subscribe(data =>{
     console.log(data);
     this.getClients();
    },
    error=>console.log(error));
  }
  updateClient(id: number){
    this.router.navigate(['client/addClient',id]);
  }
  addClient(){
    this.router.navigate(['client/addClient']);
  }
  clientList(){
    this.router.navigate(['client/clients']);
  }
  reloadComponent() {
    let currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
    }
  
  private getClients(){
    this.clientService.getClientList().subscribe(data => {
      this.clients = data;
      console.log(data);
    });
  }
  home() {
    this.router.navigate(['home']);
  }
}
