// In this way you will disable the strict checking property initialization 
// for all the project. It's better to add the ! postfix operator to the variable name,
//  to just ignore this case,
//  or initialize the variable inside the constructor

export class Product {
    productId!: number;
    prodName!: string;
    price!: number;
    batchNo!: number;
    manufacturerName!: string;
    mfgDate!: string;
    expdate!:string;

}
