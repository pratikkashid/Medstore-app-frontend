import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dealer } from '../dealer';
import { DealerService } from '../dealer.service';
@Component({
  selector: 'app-dealer-list',
  templateUrl: './dealer-list.component.html',
  styleUrls: ['./dealer-list.component.css']
})
export class DealerListComponent implements OnInit {

  dealer: Dealer[];
  constructor(private dealerService: DealerService,
    private router:Router) { }

  ngOnInit(): void {
    this.getDealer();
  }
  addDealer(){
    this.router.navigate(['create-dealer']);
  }
  dealerList(){
    this.router.navigate(['dealer']);
  }
  private getDealer(){
    this.dealerService.getDealerList().subscribe(data =>{
      this.dealer=data;
    });
  }
  dealerDetails(dealerId :number){
    this.router.navigate(['dealer-details',dealerId]);
  }
  updateDealer(dealerId:number){
    this.router.navigate(['update-dealer',dealerId]);
  }
 deleteDealer(dealerId:number){
   this.dealerService.deleteDealer(dealerId).subscribe(data =>{
     console.log(data);
     this.getDealer();
   });
 }

}
