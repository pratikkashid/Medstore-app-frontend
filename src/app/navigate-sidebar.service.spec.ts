import { TestBed } from '@angular/core/testing';

import { NavigateSidebarService } from './navigate-sidebar.service';

describe('NavigateSidebarService', () => {
  let service: NavigateSidebarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavigateSidebarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
