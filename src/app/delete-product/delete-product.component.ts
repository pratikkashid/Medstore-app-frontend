import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.css']
})
export class DeleteProductComponent implements OnInit {
  product:Product=new Product();
 id!: number;
  constructor(private route:ActivatedRoute, private router:Router,
    private productservice:ProductService) { 
      this.id=this.route.snapshot.params['id'];
    }

  ngOnInit(): void {
  }
  deleteitem(){
this.productservice.deleteProduct(this.id).subscribe((data:any)=>{
  console.log(data);
  this.gotoList();
})
  }

  gotoList(){
    this.router.navigate(['getall']);
  }
}
