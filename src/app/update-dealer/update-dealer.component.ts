import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Dealer } from '../dealer';
import { DealerService } from '../dealer.service';

@Component({
  selector: 'app-update-dealer',
  templateUrl: './update-dealer.component.html',
  styleUrls: ['./update-dealer.component.css']
})
export class UpdateDealerComponent implements OnInit {

  dealerId: number;
  dealer : Dealer=new Dealer();
  constructor(private dealerService: DealerService,
    private route:ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
    this.dealerId=this.route.snapshot.params['dealerId'];
    this.dealerService.getDealerById(this.dealerId).subscribe(data =>{
      this.dealer=data;
    },error =>console.log(error));
  }
  // saveDealer(){
  //   this.dealerService.createDealer(this.dealer).subscribe(data=>{
  //     console.log(data);
  //     this.goToDealerList();
  //   },
  //   error=>console.log(error));
  // }
  
  onSubmit(){
    this.dealerService.updateDealer(this.dealerId,this.dealer).subscribe(data=>{
      this.goToDealerList();
    },
     error=>console.log(error));
  }
  goToDealerList(){
    this.router.navigate(['/dealer'])

  }

}
