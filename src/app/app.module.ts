
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgxPaginationModule } from 'ngx-pagination';
import { SidebarModule } from '@syncfusion/ej2-angular-navigations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientListComponent } from './client-list/client-list.component';
import { CreateClientComponent } from './create-client/create-client.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AddProductComponent } from './add-product/add-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { CreateDealerComponent } from './create-dealer/create-dealer.component';
import { UpdateDealerComponent } from './update-dealer/update-dealer.component';
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { DealerDetailsComponent } from './dealer-details/dealer-details.component';
import { SearchComponent } from './search/search.component';
import { BillComponent } from './bill/bill.component';
import { ContactComponent } from './contact/contact.component';



@NgModule({
  declarations: [
    AppComponent,
    ClientListComponent,
    CreateClientComponent,
    HomeComponent,
    LoginComponent,
    ProductListComponent,
    AddProductComponent,
    DeleteProductComponent,
    EditproductComponent,
    CreateDealerComponent,
    DealerListComponent,
    UpdateDealerComponent,
    DealerDetailsComponent,
    SearchComponent,
    BillComponent,
    ContactComponent
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    // NgxPaginationModule,
    SidebarModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
