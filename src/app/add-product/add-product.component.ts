import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

product:Product=new Product();

  constructor(private productservice:ProductService,
    private router:Router
    ) { }

  ngOnInit(): void {
  }


saveProduct(){
  this.productservice.addProduct(this.product).subscribe(data=>{
    console.log(data);
    this.directToProductlist();
  },(message:any)=>console.error(message)
   )
}

directToProductlist(){
  this.router.navigate(['/getall']);
}
onSubmit(){
  console.log(this.product);
  this.saveProduct();
  }

  clientList() {
    this.router.navigate(['client/clients']);
  }
  login() {
    this.router.navigate(['login']);
  }
  products() {
    this.router.navigate(['getall']);
  }
  dealer() {
    this.router.navigate(['dealer']);
  }
  bill() {
    this.router.navigate(['search']);
  }
  LogOut(){
    this.router.navigate(['login']);
  }
  Home(){
    this.router.navigate(['home']);
  }
  addProduct(){
    this.router.navigate(['/AddProduct']);
  }
  productList(){
    this.router.navigate(['/getall']);
  }

  contactus(){
    this.router.navigate(['/contactus']);
  }
  
 redirectToEdit(id:number){
   this.router.navigate(['/editproduct',id]);
 }

 redirectTodelete(id:number){
   this.router.navigate(['/deleteproduct',id]);
 }
}
