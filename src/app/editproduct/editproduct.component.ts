import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-editproduct',
  templateUrl: './editproduct.component.html',
  styleUrls: ['./editproduct.component.css']
})
export class EditproductComponent implements OnInit {
   product:Product=new Product();
   id!: number;
  constructor( private productservice:ProductService,
    private router:ActivatedRoute,private route:Router
    ) { }

  ngOnInit(): void {
    this.id=this.router.snapshot.params[`id`];
    this.productservice.getProductById(this.id).subscribe(data=>{
      this.product=data;
      console.log(data);

    });
  }
  updateProduct(){
this.productservice.editProduct(this.id,this.product).subscribe(data=>{
  this.product=new Product();
  console.log(data);
  this.directToProductlist();

})
  }
  onSubmit(){
this.updateProduct();
  }
directToProductlist(){
  this.route.navigate(['/getall']);
}
}
