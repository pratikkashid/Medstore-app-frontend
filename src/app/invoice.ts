import { Client } from "./client";
import { Product } from "./product";

export class Invoice {

        client!:Client
        invoiceNo!:number;
        discount!:number;
        totalPrice!:number;
        products!:Product[];
      
}

