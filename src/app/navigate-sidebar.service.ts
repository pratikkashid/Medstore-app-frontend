import { Injectable } from '@angular/core';
import { SidebarAllModule } from '@syncfusion/ej2-angular-navigations';

@Injectable({
  providedIn: 'root'
})
export class NavigateSidebarService {

  constructor() { }
  showSidebar: boolean = false;

    toggleSideBar() {
        this.showSidebar = !this.showSidebar;
    }
}
