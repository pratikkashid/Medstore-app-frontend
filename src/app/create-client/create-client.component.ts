import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.css']
})
export class CreateClientComponent implements OnInit {
  id: number;
  client: Client=new Client();
  constructor(private clientService: ClientService,
    private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.clientService.getClientById(this.id).subscribe(data=>{
      this.client = data;},
      error=>console.log(error));
  }

  saveClient(){
    this.clientService.createClient(this.client).subscribe(data =>{
      
     console.log(data);
     this.goToClientList();
    },
    error=>console.log(error));
  }
  goToClientList(){
    this.router.navigate(['/client/clients']);
  }
  onSubmit(){
    console.log(this.client);
    this.saveClient();
  }

}
