import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './product';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private baseurl='http://localhost:8084/getallproduct';
  private addurl="http://localhost:8084/AddProduct";
  private getbyidurl="http://localhost:8084/getbyid";
  private editproducturl="http://localhost:8084/updateProduct";
  private deleteurl="http://localhost:8084/deleteproduct";
  private searchurl="http://localhost:8084//search";
  constructor(private httpclient:HttpClient) { }
  
  getProductList():Observable<Product[]>{
    return this.httpclient.get<Product[]>(`${this.baseurl}`);
  }

  addProduct(product:Product):Observable<any>{
return this.httpclient.post(`${this.addurl}`,product);
  }
  getProductById(id:number):Observable<Product>{
    return this.httpclient.get<Product>(`${this.getbyidurl}/${id}`);
  }

  editProduct(id:number,product:Product):Observable<any>{
    return this.httpclient.put(`${this.editproducturl}/${id}`,product);
  }
  deleteProduct(id:number):Observable<Object>{
    return this.httpclient.delete(`${this.deleteurl}/${id}`);
  }
   
  searchProduct(keyword:string):Observable<Product[]>{
    return this.httpclient.get<Product[]>(this.searchurl+`?keyword=`+keyword);
  }
// You are returning Observable<Product> and expecting it to be Product[] inside subscribe callback.
// The Type returned from http.get() and getProducts() should be Observable<Product[]>
}
