import { Component, OnInit } from '@angular/core';

import { ProductService } from '../product.service';

import * as pdfFonts from "pdfmake/build/vfs_fonts";

import * as pdfMake from 'pdfmake/build/pdfmake';

import { Product } from '../product';
import { Invoice } from '../invoice';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  invoice = new Invoice();
  key!:string;
  products: Product[] = [];
  items: Product[]=[];
  totalPrice: number =0;
  constructor(private productservice:ProductService) { }

  ngOnInit(): void {
   
  }
 searchdata(key: string){
this.productservice.searchProduct(key).subscribe(data=>{
  this.products=data;
})
}
addToBill(item: Product){
  
this.items.push(item);
console.log(this.items);

}
generateBill(){
  this.totalPrice =0;
  for(let item of this.items)
  {
  this.totalPrice+=item.price;
  }
}
clear()
{
  this.items = [];
}

}
